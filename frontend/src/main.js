import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import "normalize.css";
import Router from "vue-router";
import BookGrid from "./components/Book/BookGrid";
import BookDetail from "./components/Book/BookDetail";
import AuthorList from "./components/Author/AuthorList";
import {store} from "./store/store";

Vue.use(Vuex);
Vue.use(Router);

Vue.config.productionTip = false;

const routes = [
    {
        path: "/",
        component: BookGrid
    },
    {
        path: "/books",
        component: BookGrid
    },
    {
        path: "/book/:id",
        component: BookDetail,
        name: "BookDetail"
    },
    {
        path: "/authors",
        component: AuthorList
    }
];

const router = new Router({
  routes
});

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app');
