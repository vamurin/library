import Vuex from "vuex";
import Vue from "vue";
import {bookListService} from "../services/bookListService"

Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        books: {map: new Map(), bookList: [], filter: "", sort: "", page: 1},
        authors: {map: new Map(), authorList: []},
        publishing: {map: new Map(), publishingList: []},
    },
    mutations: {
        SET_BOOKS(state, books) {
            state.books.map = new Map(state.books.map);
            for (let book of books) {
                state.books.map.set(book.id, book);
            }
            state.books.bookList = books.map(item => item.id);

        }
    },
    actions: {
        async fetchBooks(context) {
            context.commit('SET_BOOKS', await bookListService.fetchBooks({
                page:context.state.books.page,
                filter:context.state.books.filter,
                sort:context.state.books.sort
            }));
        }
    },
    getters: {
        books: (state) => {
            return state.books;
        },
        getBook: state => id => {
            return state.books.map.get(id);
        }
    }
});