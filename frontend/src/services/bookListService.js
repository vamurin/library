import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {API_URL} from "./api";

Vue.use(VueAxios, axios);

const booksApi = API_URL.host+API_URL.books;

export const bookListService = {
    fetchBooks: function(options = {page: 1, sort: "", filter: ""}) {
        let queryString = '';
        if(options.page)
            queryString += `page=${options.page}&`;
        if(options.sort)
            queryString += `sort=${options.sort}&`;
        if(options.filter)
            queryString += `filter=${options.filter}`;
        return Vue.axios.get(`${booksApi}?${queryString}`).then(function (response) {
            return response.data;
        });
    }
};