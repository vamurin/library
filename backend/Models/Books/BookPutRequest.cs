﻿using library.Helpers;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using library.ServiceExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace library.Models.Books
{
    public class BookPutRequest
    {
        [FromForm(Name = "isbn")]
        public string ISBN { get; set; }
        [FromForm(Name = "author_id")]
        public int AuthorId { get; set; }
        [FromForm(Name = "name")]
        public string Name { get; set; }
        [FromForm(Name = "genre_id")]
        public int GenreId { get; set; }
        [FromForm(Name = "publisher_id")]
        public int PublisherId { get; set; }
        [FromForm(Name = "year")]
        public string Year { get; set; }
        [FromForm(Name = "description")]
        public string Description { get; set; }
        [FromForm(Name = "images")]
        public IFormFile[] Images { get; set; }

        public async Task<BookNhibernate> ToNhibernateBook(IAuthorStorage<AuthorNhibernate> _authorStorage,
            IPublishHouseStorage<PublishHouseNhibernate> _publisherStorage,
            IGenreStorage<GenreNhibernate> _genreStorage)
        {
            var bookDB = new BookNhibernate();

            bookDB.Isbn = ISBN;
            bookDB.Author = await _authorStorage.GetById(AuthorId);
            // TODO: if authour is null, throw an exception    
            bookDB.Name = Name;
            bookDB.Genre = await _genreStorage.GetById(GenreId);
            // TODO: if genre is null, throw an exception    
            bookDB.PublishHouse = await _publisherStorage.GetById(PublisherId);
            // TODO: if publishhouse is null, throw an exception
            bookDB.PublishYear = DateTime.ParseExact(Year, SystemDefinitions.bookPublishDateFormat, null);
            bookDB.Description = Description;

            return bookDB;
        }

        async public Task<List<ImageNhibernate>> ToNhibernateImages(BookNhibernate bookDB, FileStorage fileStorage)
        {
            if (Images == null || Images.Length == 0)
                return null;

            var result = new List<ImageNhibernate>();
            foreach (var image in Images)
            {
                result.Add(new ImageNhibernate()
                {
                    Book = bookDB,
                    Image = await fileStorage.WriteFormFile(image)
                });
            }

            return result;
        }
    }
}
