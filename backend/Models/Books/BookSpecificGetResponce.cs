﻿using library.backend.Helpers;
using library.Models.Database.NhibernateMap;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace library.Models.Books
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BookSpecificGetResponce
    {
        public int Id { get; set; }
        public string ISBN { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public List<SingleGenre> AllGenres { get; set; }
        public string PublisherName { get; set; }
        public int PublisherId { get; set; }
        [JsonConverter(typeof(YearJsonConverter))]
        public DateTime Year { get; set; }
        public string Description { get; set; }
        public List<string> ImageUrls { get; set; }

        public BookSpecificGetResponce(BookNhibernate bookDB, List<ImageNhibernate> imagesDB, List<GenreNhibernate> genresDB)
        {
            Id = bookDB.Id;
            ISBN = bookDB.Isbn;
            AuthorId = bookDB.Author.Id;
            AuthorName = $"{bookDB.Author.FirstName}{((bookDB.Author.MiddleName != null) ? $" {bookDB.Author.MiddleName} " : " ")}{bookDB.Author.LastName}";
            Name = bookDB.Name;
            Genre = bookDB.Genre.Genre;
            PublisherName = bookDB.PublishHouse.Name;
            PublisherId = bookDB.PublishHouse.Id;
            Year = bookDB.PublishYear;
            Description = bookDB.Description;

            ImageUrls = new List<string>();
            imagesDB.ForEach(x => ImageUrls.Add(x.Image));

            AllGenres = new List<SingleGenre>();
            genresDB.ForEach(x => AllGenres.Add(new SingleGenre() { Id = x.Id, Name = x.Genre }));

        }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SingleGenre
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
