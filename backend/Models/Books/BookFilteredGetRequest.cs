﻿using Microsoft.AspNetCore.Mvc;

namespace library.Models.Books
{
    public class BookFilteredGetRequest
    {
        [FromQuery(Name = "filter")]
        public string Filter { get; set; }
        [FromQuery(Name = "sort")]
        public string Sort { get; set; }
        [FromQuery(Name = "page")]
        public int Page { get; set; }
    }
}
