﻿using library.backend.Helpers;
using library.Models.Database.NhibernateMap;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace library.Models.Books
{

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BookFilteredGetResponce
    {
        public int Id { get; set; }
        public string ISBN { get; set; }
        public string AuthorName { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public string PublisherName { get; set; }
        [JsonConverter(typeof(YearJsonConverter))]
        public DateTime Year { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public BookFilteredGetResponce(BookNhibernate bookDB, ImageNhibernate imageDB)
        {
            Id = bookDB.Id;
            ISBN = bookDB.Isbn;
            AuthorName = $"{bookDB.Author.FirstName}{((bookDB.Author.MiddleName != null) ? $" {bookDB.Author.MiddleName} " : " ")}{bookDB.Author.LastName}";
            Name = bookDB.Name;
            Genre = bookDB.Genre.Genre;
            PublisherName = bookDB.PublishHouse.Name;
            Year = bookDB.PublishYear;
            Description = bookDB.Description;

            if (imageDB != null)
                ImageUrl = imageDB.Image;
        }
    }
}
