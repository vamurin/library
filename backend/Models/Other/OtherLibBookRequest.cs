﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace library.Models.Other
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class OtherLibBookRequest
    {
        public string ISBN { get; set; }
    }
}
