﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace library.Models.Other
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class OtherLibBookResponce
    {
        public string LibraryName { get; set; }
        public string BookName { get; set; }
        public string Url { get; set; }
    }
}
