﻿using library.backend.Helpers;
using library.Models.Database.NhibernateMap;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace library.Models.Authors
{

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class AuthorGetResponce
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        [JsonConverter(typeof(YearMonthDayJsonConverter))]
        public DateTime BirthDate { get; set; }
        public string PhotoUrl { get; set; }

        public AuthorGetResponce(AuthorNhibernate authorDB)
        {
            Id = authorDB.Id;
            FirstName = authorDB.FirstName;
            MiddleName = authorDB.MiddleName;
            LastName = authorDB.LastName;
            BirthDate = authorDB.BirthDate;
            PhotoUrl = authorDB.Photo;
        }

    }
}
