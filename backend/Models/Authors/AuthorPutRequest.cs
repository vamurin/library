﻿using library.Helpers;
using library.Models.Database.NhibernateMap;
using library.ServiceExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace library.Models.Authors
{
    public class AuthorPutRequest
    {
        [FromForm(Name = "first_name")]
        public string FirstName { get; set; }
        [FromForm(Name = "last_name")]
        public string LastName { get; set; }
        [FromForm(Name = "middle_name")]
        public string MiddleName { get; set; }
        [FromForm(Name = "birth_date")]
        public string BirthDate { get; set; }
        [FromForm(Name = "photo")]
        public IFormFile Photo { get; set; }

        async public Task<AuthorNhibernate> ToNhibernate(FileStorage fileStorage)
        {
            var authorDB = new AuthorNhibernate();
            authorDB.FirstName = FirstName;
            authorDB.LastName = LastName;
            authorDB.MiddleName = MiddleName;
            authorDB.BirthDate = DateTime.ParseExact(BirthDate, SystemDefinitions.authorBirthDateFormat, null);

            if (Photo == null || Photo.Length == 0)
                authorDB.Photo = "";
            else
            {
                authorDB.Photo = await fileStorage.WriteFormFile(Photo);
            }
            return authorDB;
        }
    }
}
