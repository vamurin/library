﻿using Microsoft.AspNetCore.Mvc;

namespace library.Models.Authors
{
    public class AuthorGetRequest
    {
        [FromQuery(Name = "filter")]
        public string Filter { get; set; }
        [FromQuery(Name = "sort")]
        public string Sort { get; set; }
        [FromQuery(Name = "page")]
        public int Page { get; set; }
    }
}
