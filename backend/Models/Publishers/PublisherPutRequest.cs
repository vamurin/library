﻿using System;
using library.Models.Database.NhibernateMap;
using Microsoft.AspNetCore.Mvc;

namespace library.Models.Publishers
{
    public class PublisherPutRequest
    {
        [FromForm(Name = "name")]
        public string Name { get; set; }
        [FromForm(Name = "address")]
        public string Address { get; set; }
        [FromForm(Name = "code")]
        public string Code { get; set; }
        [FromForm(Name = "description")]
        public string Description { get; set; }

        public PublishHouseNhibernate ToNhibernate()
        {
            var PublishHouseNH = new PublishHouseNhibernate();

            PublishHouseNH.Address = Address;
            PublishHouseNH.Code = Convert.ToInt32(Code);
            PublishHouseNH.Description = Description;
            PublishHouseNH.Name = Name;

            return PublishHouseNH;
        }
    }
}
