﻿using System;
using System.Collections.Generic;
using System.Linq;
using library.Models.Database.NhibernateMap;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace library.Models.Publishers
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class PublisherGetResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public  string Description { get; set; }
        
        public PublisherGetResponse(PublishHouseNhibernate publisherNH)
        {
            Id = publisherNH.Id;
            Name = publisherNH.Name;
            Address = publisherNH.Address;
            Code = Convert.ToString(publisherNH.Code);
            Description = publisherNH.Description;
        }
    }
}