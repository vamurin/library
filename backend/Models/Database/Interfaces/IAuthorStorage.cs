using library.Models.Database.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace library.Models.Database.Interfaces
{
    public interface IAuthorStorage<T>
    {
        Task Add(T obj);
        Task Delete(T obj);
        Task<T> GetById(int id);
        Task<List<T>> GetAll(AuthorFilter filter);
        Task Update(T obj);
    }
}