using System.Collections.Generic;
using System.Threading.Tasks;
using library.Models.Database.Filters;

namespace library.Models.Database.Interfaces
{
    public interface IPublishHouseStorage<T>
    {
        Task Add(T obj);
        Task Delete(T obj);
        Task<T> GetById(int id);
        Task<List<T>> GetAll(PublishHouseFilter filter);
        Task Update(T obj);
    }
}