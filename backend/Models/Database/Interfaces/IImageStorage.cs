using System.Collections.Generic;
using System.Threading.Tasks;

namespace library.Models.Database.Interfaces
{
    public interface IImageStorage<T, B>
    {
        Task Add(T obj);
        Task Delete(T obj);
        Task<T> GetById(int id);
        Task<List<T>> GetAll();
        Task<List<T>> GetByBook(B book);
        Task Update(T obj);
    }
}