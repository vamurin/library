using System.Collections.Generic;
using System.Threading.Tasks;

namespace library.Models.Database.Interfaces
{
    public interface IGenreStorage<T>
    {
        Task Add(T obj);
        Task Delete(T obj);
        Task<T> GetById(int id);
        Task<List<T>> GetAll();
        Task Update(T obj);
    }
}