using library.Models.Database.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace library.Models.Database.Interfaces
{
    public interface IBookStorage<B, A, P, G>
    {
        Task Add(B obj);
        Task Delete(B obj);
        Task<B> GetById(int id);
        Task<List<B>> GetAll(BookFilter filter);
        Task Update(B obj);
        Task<B> GetByISBN(string isbn);
        Task<List<B>> GetByAuthor(A author);
        Task<List<B>> GetByPublishHouse(P publishHouse);
        Task<List<B>> GetRandomByGenre(int numberToTake, G genre);
        Task<List<B>> GetRandomByAuthor(int numberToTake, A author);
        Task<List<B>> GetLatestByPublisher(int numberToTake, P publishHouse);
    }
}