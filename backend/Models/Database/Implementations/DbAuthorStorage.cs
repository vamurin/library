using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using Microsoft.Extensions.Configuration;
using NHibernate.Criterion;
using ISession = NHibernate.ISession;

namespace library.Models.Database.Implementations
{
    public class DbAuthorStorage : IAuthorStorage<AuthorNhibernate>
    {
        private readonly ISession _session;
        private readonly IConfiguration _conf;

        public DbAuthorStorage(ISession session, IConfiguration conf)
        {
            _session = session;
            _conf = conf;
        }

        public async Task Add(AuthorNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.SaveAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere

                throw;
            }
        }

        public async Task Delete(AuthorNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.DeleteAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task<AuthorNhibernate> GetById(int id)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var author = await _session.GetAsync<AuthorNhibernate>(id);
                    await tx.CommitAsync();
                    return author;
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task<List<AuthorNhibernate>> GetAll(AuthorFilter filter)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var query = _session.QueryOver<AuthorNhibernate>();
                    // search by author name
                    if (filter.Names != null && filter.Names.Length > 0) {
                        var restrictions = Restrictions.Disjunction();
                        foreach (var name in filter.Names)
                        {
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.FirstName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.MiddleName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.LastName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                        }

                        _ = query.Where(restrictions);
                    }

                    if (filter.MinYear != null)
                        _ = query.Where(c => c.BirthDate >= filter.MinYear);

                    if (filter.MaxYear != null)
                        _ = query.Where(c => c.BirthDate <= filter.MaxYear);

                    switch (filter.SortParam)
                    {
                        case AuthorFilter.SortParamEnum.ByAuthor when filter.SortOrder == AuthorFilter.SortOrderEnum.Asc:
                            _ = query.OrderBy(c => c.FirstName).Asc.
                                    ThenBy(c => c.MiddleName).Asc.
                                    ThenBy(c => c.LastName).Asc.
                                    ThenBy(c => c.Id).Asc;
                            break;
                        case AuthorFilter.SortParamEnum.ByAuthor:
                            _ = query.OrderBy(c => c.FirstName).Desc.
                                    ThenBy(c => c.MiddleName).Desc.
                                    ThenBy(c => c.LastName).Desc.
                                    ThenBy(c => c.Id).Desc;
                            break;
                        case AuthorFilter.SortParamEnum.ByYear when filter.SortOrder == AuthorFilter.SortOrderEnum.Asc:
                            _ = query.OrderBy(c => c.BirthDate).Asc.
                                    ThenBy(c => c.Id).Asc;
                            break;
                        case AuthorFilter.SortParamEnum.ByYear:
                            _ = query.OrderBy(c => c.BirthDate).Desc.
                                    ThenBy(c => c.Id).Desc;
                            break;
                        default:
                            _ = query.OrderBy(c => c.Id).Asc;
                            break;
                    }

                    if (filter.Page != AuthorFilter.NoPaging)
                    {
                        var curMaxRowsPerPage = _conf.GetValue<int>(SystemDefinitions.appSettingsRowsPerPage);

                        _ = query.Skip((filter.Page - 1) * curMaxRowsPerPage).Take(curMaxRowsPerPage);
                    }

                    var result = await query.ListAsync();
                    await tx.CommitAsync();
                    return result.ToList();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n") ;
                return null;
            }
        }

        public async Task Update(AuthorNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.UpdateAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere

                throw;
            }
        }
    }
}