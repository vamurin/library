using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using ISession = NHibernate.ISession;

namespace library.Models.Database.Implementations
{
    public class DbGenreStorage : IGenreStorage<GenreNhibernate>
    {
        private readonly ISession _session;

        public DbGenreStorage(ISession session)
        {
            _session = session;
        }
        
        public async Task Add(GenreNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.SaveAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task Delete(GenreNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.DeleteAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task<GenreNhibernate> GetById(int id)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var genre = await _session.GetAsync<GenreNhibernate>(id);
                    await tx.CommitAsync();
                    return genre;
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task<List<GenreNhibernate>> GetAll()
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var list = await _session.QueryOver<GenreNhibernate>().ListAsync();
                    await tx.CommitAsync();
                    return list.ToList();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task Update(GenreNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.UpdateAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }
    }
}