using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using Microsoft.Extensions.Configuration;
using ISession = NHibernate.ISession;

namespace library.Models.Database.Implementations
{
    public class DbPublishHouseStorage : IPublishHouseStorage<PublishHouseNhibernate>
    {
        private readonly ISession _session;
        private readonly IConfiguration _conf;

        public DbPublishHouseStorage(ISession session, IConfiguration conf)
        {
            _session = session;
            _conf = conf;
        }

        public async Task Add(PublishHouseNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.SaveAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task Delete(PublishHouseNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.DeleteAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task<PublishHouseNhibernate> GetById(int id)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var publishHouse = await _session.GetAsync<PublishHouseNhibernate>(id);
                    await tx.CommitAsync();
                    return publishHouse;
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task<List<PublishHouseNhibernate>> GetAll(PublishHouseFilter filter)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    if (filter.Name != "")
                        _session.EnableFilter("publishHouseNameFilter").SetParameter("filterName", $"%{filter.Name}%");
                    else
                        _session.DisableFilter("publishHouseNameFilter");

                    if (filter.Code != null)
                        _session.EnableFilter("publishHouseCodeFilter").SetParameter("filterCode", filter.Code);
                    else
                        _session.DisableFilter("publishHouseCodeFilter");

                    var query = _session.QueryOver<PublishHouseNhibernate>();

                    switch (filter.SortParam)
                    {
                        case PublishHouseFilter.SortParamEnum.ByCode when filter.SortOrder == PublishHouseFilter.SortOrderEnum.Asc:
                            _ = query.OrderBy(c => c.Code).Asc.
                                    ThenBy(c => c.Id).Asc;
                            break;
                        case PublishHouseFilter.SortParamEnum.ByCode:
                            _ = query.OrderBy(c => c.Code).Desc.
                                    ThenBy(c => c.Id).Desc;
                            break;
                        case PublishHouseFilter.SortParamEnum.ByName when filter.SortOrder == PublishHouseFilter.SortOrderEnum.Asc:
                            _ = query.OrderBy(c => c.Name).Asc.
                                    ThenBy(c => c.Id).Asc;
                            break;
                        case PublishHouseFilter.SortParamEnum.ByName:
                            _ = query.OrderBy(c => c.Name).Desc.
                                    ThenBy(c => c.Id).Desc;
                            break;
                        default:
                            _ = query.OrderBy(c => c.Id).Asc;
                            break;
                    }

                    if (filter.Page != PublishHouseFilter.NoPaging)
                    {
                        var curMaxRowsPerPage = _conf.GetValue<int>(SystemDefinitions.appSettingsRowsPerPage);

                        _ = query.Skip((filter.Page - 1) * curMaxRowsPerPage).Take(curMaxRowsPerPage);
                    }

                    var result = await query.ListAsync();
                    await tx.CommitAsync();
                    return result.ToList();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task Update(PublishHouseNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.UpdateAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }
    }
}