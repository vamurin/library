using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using ISession = NHibernate.ISession;

namespace library.Models.Database.Implementations
{
    public class DbImageStorage : IImageStorage<ImageNhibernate, BookNhibernate>
    {
        private readonly ISession _session;

        public DbImageStorage(ISession session)
        {
            _session = session;
        }
        
        public async Task Add(ImageNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.SaveAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task Delete(ImageNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.DeleteAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        public async Task<List<ImageNhibernate>> GetByBook(BookNhibernate book)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var result = await _session.QueryOver<ImageNhibernate>().Where(x => x.Book == book).ListAsync();
                    await tx.CommitAsync();
                    return result.ToList();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                throw;
            }
        }

        public async Task<ImageNhibernate> GetById(int id)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var image = await _session.GetAsync<ImageNhibernate>(id);
                    await tx.CommitAsync();
                    return image;
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                throw;
            }
        }

        public async Task<List<ImageNhibernate>> GetAll()
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var list = await _session.QueryOver<ImageNhibernate>().ListAsync();
                    await tx.CommitAsync();
                    return list.ToList();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }

        public async Task Update(ImageNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.UpdateAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }
    }
}