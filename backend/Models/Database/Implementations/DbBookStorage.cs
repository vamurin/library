using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using Microsoft.Extensions.Configuration;
using NHibernate.Criterion;
using ISession = NHibernate.ISession;

namespace library.Models.Database.Implementations
{
    public class DbBookStorage : IBookStorage<BookNhibernate,AuthorNhibernate,PublishHouseNhibernate,GenreNhibernate>
    {
        private readonly ISession _session;
        private readonly IConfiguration _conf;

        public DbBookStorage(ISession session, IConfiguration conf)
        {
            _session = session;
            _conf = conf;
        }

        public async Task Add(BookNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.SaveAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
            }
        }

        public async Task Delete(BookNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.DeleteAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
            }
        }

        public async Task<BookNhibernate> GetById(int id)
        {
            try
            {                
                using (var tx = _session.BeginTransaction())
                {
                    var book = await _session.GetAsync<BookNhibernate>(id);
                    await tx.CommitAsync();
                    return book;
                }
                
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }

        public async Task<List<BookNhibernate>> GetByAuthor(AuthorNhibernate author)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var list = await _session.QueryOver<BookNhibernate>().Where(x => x.Author == author).ListAsync();
                    await tx.CommitAsync();
                    return list.ToList();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }
        
        public async Task<BookNhibernate> GetByISBN(string isbn)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var book = await _session.QueryOver<BookNhibernate>().Where(x => x.Isbn == isbn).SingleOrDefaultAsync();
                    await tx.CommitAsync();
                    return book;
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }
        
        public async Task<List<BookNhibernate>> GetByPublishHouse(PublishHouseNhibernate publishHouse)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var list = await _session.QueryOver<BookNhibernate>().Where(x => x.PublishHouse == publishHouse).ListAsync();
                    await tx.CommitAsync();
                    return list.ToList();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }

        public async Task<List<BookNhibernate>> GetAll(BookFilter filter)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var query = _session.QueryOver<BookNhibernate>();
                    // search by author name
                    if (filter.Names != null && filter.Names.Length > 0)
                    {
                        var restrictions = Restrictions.Disjunction();
                        foreach (var name in filter.Names)
                        {
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.FirstName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.MiddleName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                            restrictions.Add(Restrictions.On<AuthorNhibernate>(c => c.LastName)
                              .IsInsensitiveLike(name, MatchMode.Anywhere));
                        }

                        _ = query.JoinQueryOver(c => c.Author).Where(restrictions);
                    }

                    if (filter.Year != null)
                        _ = query.Where(c => c.PublishYear == filter.Year);

                    if (filter.GenreId != null)
                    {
                        _ = query.Where(c => c.Genre.Id == filter.GenreId);
                    }

                    switch (filter.SortParam)
                    {
                        case BookFilter.SortParamEnum.ByAuthor when 
                        (filter.SortOrder == BookFilter.SortOrderEnum.Asc):
                            _ = query.JoinQueryOver(c => c.Author).OrderBy(a => a.FirstName).Asc.
                                    ThenBy(a => a.MiddleName).Asc.
                                    ThenBy(a => a.LastName).Asc.
                                    ThenBy(a => a.Id).Asc;
                            break;
                        case BookFilter.SortParamEnum.ByAuthor:
                            _ = query.JoinQueryOver(c => c.Author).OrderBy(a => a.FirstName).Desc.
                                    ThenBy(a => a.MiddleName).Desc.
                                    ThenBy(a => a.LastName).Desc.
                                    ThenBy(a => a.Id).Desc;
                            break;
                        case BookFilter.SortParamEnum.ByYear when filter.SortOrder == BookFilter.SortOrderEnum.Asc:
                            _ = query.OrderBy(c => c.PublishYear).Asc.
                                    ThenBy(c => c.Id).Asc;
                            break;
                        case BookFilter.SortParamEnum.ByYear:
                            _ = query.OrderBy(c => c.PublishYear).Desc.
                                    ThenBy(c => c.Id).Desc;
                            break;
                        case BookFilter.SortParamEnum.ByGenre when filter.SortOrder == BookFilter.SortOrderEnum.Asc:
                            _ = query.JoinQueryOver(c => c.Genre).OrderBy(g => g.Genre).Asc.
                                    ThenBy(g => g.Id).Asc;
                            break;
                        case BookFilter.SortParamEnum.ByGenre:
                            _ = query.JoinQueryOver(c => c.Genre).OrderBy(g => g.Genre).Desc.
                                    ThenBy(g => g.Id).Desc;
                            break;
                        default:
                            _ = query.OrderBy(c => c.Id).Asc;
                            break;
                    }

                    if (filter.Page != BookFilter.NoPaging)
                    {
                        var curMaxRowsPerPage = _conf.GetValue<int>(SystemDefinitions.appSettingsRowsPerPage);

                        _ = query.Skip((filter.Page - 1) * curMaxRowsPerPage).Take(curMaxRowsPerPage);
                    }

                    var result = await query.ListAsync();
                    await tx.CommitAsync();
                    return result.ToList();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }

        public async Task Update(BookNhibernate obj)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    await _session.UpdateAsync(obj);
                    await tx.CommitAsync();
                }
            }
            catch
            {
                // TODO: log exceptions somewhere
            }
        }

        private List<T> GetRandomItems<T>(List<T> list, int numberToTake)
        {
            // select random 'number' of elements
            if (numberToTake > 0 && (list != null || list.Count > 0))
            {
                var selected = new List<T>();
                double needed = numberToTake;
                double available = list.Count;
                int maxCount = (list.Count < numberToTake) ? list.Count : numberToTake;
                var rand = new Random();
                while (selected.Count < maxCount)
                {
                    if (rand.NextDouble() < needed / available)
                    {
                        selected.Add(list[(int)available - 1]);
                        needed--;
                    }
                    available--;
                }
                return selected;
            }
            return list;
        }

        public async Task<List<BookNhibernate>> GetRandomByGenre(int numberToTake, GenreNhibernate genre)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    // TODO: make selection of random number of rows as a view in PostgreSQL and get it using
                    // standard nhibernate instead of doing randomness on the server side
                    var query = _session.QueryOver<BookNhibernate>();

                    _ = query.Where(c => c.Genre.Id == genre.Id);

                    var result = await query.ListAsync();
                    await tx.CommitAsync();

                    // select random 'number' of elements
                    return GetRandomItems(result.ToList(), numberToTake);
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }

        public async Task<List<BookNhibernate>> GetRandomByAuthor(int numberToTake, AuthorNhibernate author)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    // TODO: make selection of random number of rows as a view in PostgreSQL and get it using
                    // standard nhibernate instead of doing randomness on the server side
                    var query = _session.QueryOver<BookNhibernate>();

                    _ = query.Where(c => c.Author.Id == author.Id);

                    var result = await query.ListAsync();
                    await tx.CommitAsync();

                    // select random 'number' of elements
                    return GetRandomItems(result.ToList(), numberToTake);
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }

        public async Task<List<BookNhibernate>> GetLatestByPublisher(int numberToTake, PublishHouseNhibernate publishHouse)
        {
            try
            {
                using (var tx = _session.BeginTransaction())
                {
                    var list = await _session.QueryOver<BookNhibernate>().Where(x => x.PublishHouse == publishHouse).OrderBy(b => b.PublishYear).Desc.Take(numberToTake).ListAsync();
                    await tx.CommitAsync();
                    return list.ToList();
                }
            }
            catch (Exception e)
            {
                // TODO: log exceptions somewhere
                Console.WriteLine($"\n{e.Message}\n");
                return null;
            }
        }
    }
}