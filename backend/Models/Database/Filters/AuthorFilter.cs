﻿using library.Helpers;
using System;
using System.Linq;

namespace library.Models.Database.Filters
{
    public class AuthorFilter
    {
        public const int NoPaging = 0;

        public const string filterParamName = "Name";
        public const string filterParamMaxYear = "MaxYear";
        public const string filterParamMinYear = "MinYear";

        public const string sortParamByAuthor = "ByAuthor";
        public const string sortParamByYear = "ByYear";

        public const string sortValueDesc = "desc";

        public enum SortParamEnum
        {
            ByAuthor,
            ByYear,
            NoSort
        };

        public enum SortOrderEnum
        {
            Asc,
            Desc
        };

        public string[] Names { get; set; }
        public DateTime? MaxYear { get; set; }
        public DateTime? MinYear { get; set; }
        public SortParamEnum SortParam { get; set; }
        public SortOrderEnum SortOrder { get; set; }
        public int Page { get; set; }

        public AuthorFilter(string filter, string sort, int page)
        {
            ExtractFromRequest(filter, sort, page);
        }

        public void ExtractFromRequest(string filter, string sort, int page)
        {
            Names = null;

            if (filter == null)
                filter = "";

            if (sort == null)
                sort = "";

            var filterList = filter.Split(';').ToList();
            foreach (var value in filterList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case filterParamName:
                        Names = value[1].Split(null);
                        break;
                    case filterParamMaxYear:
                        MaxYear = DateTime.ParseExact(value[1], SystemDefinitions.authorBirthDateFormat, null);
                        break;
                    case filterParamMinYear:
                        MinYear = DateTime.ParseExact(value[1], SystemDefinitions.authorBirthDateFormat, null);
                        break;
                }
            }

            var sortList = sort.Split(';').ToList();

            foreach (var value in sortList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case sortParamByAuthor:
                        SortParam = SortParamEnum.ByAuthor;
                        break;
                    case sortParamByYear:
                        SortParam = SortParamEnum.ByYear;
                        break;
                    default:
                        SortParam = SortParamEnum.NoSort;
                        break;
                }

                try
                {
                    switch (value[1])
                    {
                        case sortValueDesc:
                            SortOrder = SortOrderEnum.Desc;
                            break;
                        default:
                            SortOrder = SortOrderEnum.Asc;
                            break;
                    }
                }
                catch
                {
                    // TODO: log exceptions somewhere
                }
            }

            if (page < 0)
                Page = NoPaging;
            else
                Page = page;
        }
    }
}
