﻿using library.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library.Models.Database.Filters
{
    public class BookFilter
    {
        public const int NoPaging = 0;

        public const string filterParamAuthor = "Author";
        public const string filterParamYear = "Year";
        public const string filterParamGenre = "Genre";

        public const string sortParamByAuthor = "ByAuthor";
        public const string sortParamByYear = "ByYear";
        public const string sortParamByGenre = "ByGenre";

        public const string sortValueDesc = "desc";

        public enum SortParamEnum
        {
            ByAuthor,
            ByYear,
            ByGenre,
            NoSort
        };

        public enum SortOrderEnum
        {
            Asc,
            Desc
        };

        public string[] Names { get; set; }
        public DateTime? Year { get; set; }
        public int? GenreId { get; set; }
        public SortParamEnum SortParam { get; set; }
        public SortOrderEnum SortOrder { get; set; }
        public int Page { get; set; }

        public BookFilter(string filter, string sort, int page)
        {
            ExtractFromRequest(filter, sort, page);
        }

        public void ExtractFromRequest(string filter, string sort, int page)
        {
            Names = null;

            if (filter == null)
                filter = "";

            if (sort == null)
                sort = "";

            var filterList = filter.Split(';').ToList();
            foreach (var value in filterList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case filterParamAuthor:
                        Names = value[1].Split(null);
                        break;
                    case filterParamYear:
                        Year = DateTime.ParseExact(value[1], SystemDefinitions.bookPublishDateFormat, null);
                        break;
                    case filterParamGenre:
                        GenreId = Convert.ToInt32(value[1]);
                        break;
                }
            }

            var sortList = sort.Split(';').ToList();

            foreach (var value in sortList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case sortParamByAuthor:
                        SortParam = SortParamEnum.ByAuthor;
                        break;
                    case sortParamByYear:
                        SortParam = SortParamEnum.ByYear;
                        break;
                    case sortParamByGenre:
                        SortParam = SortParamEnum.ByGenre;
                        break;
                    default:
                        SortParam = SortParamEnum.NoSort;
                        break;
                }

                try
                {
                    switch (value[1])
                    {
                        case sortValueDesc:
                            SortOrder = SortOrderEnum.Desc;
                            break;
                        default:
                            SortOrder = SortOrderEnum.Asc;
                            break;
                    }
                }
                catch
                {
                    // TODO: log exceptions somewhere
                }
            }

            if (page < 0)
                Page = NoPaging;
            else
                Page = page;
        }
    }
}
