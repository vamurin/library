using System;
using System.Linq;

namespace library.Models.Database.Filters
{
    public class PublishHouseFilter
    {
        public const int NoPaging = 0;

        public const string filterParamName = "Name";
        public const string filterParamCode = "Code";

        public const string sortParamByName = "ByName";
        public const string sortParamByCode = "ByCode";

        public const string sortValueDesc = "desc";

        public enum SortParamEnum
        {
            ByName,
            ByCode,
            NoSort
        };

        public enum SortOrderEnum
        {
            Asc,
            Desc
        };

        public string Name { get; set; }
        public int? Code { get; set; }
        public SortParamEnum SortParam { get; set; }
        public SortOrderEnum SortOrder { get; set; }
        public int Page { get; set; }

        public PublishHouseFilter(string filter, string sort, int page)
        {
            Name = "";
            Code = null;

            if (filter == null)
                filter = "";

            if (sort == null)
                sort = "";

            var filterList = filter.Split(';').ToList();
            foreach (var value in filterList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case filterParamName:
                        Name = value[1];
                        break;
                    case filterParamCode:
                        Code = Convert.ToInt32(value[1]);
                        break;
                }
            }

            var sortList = sort.Split(';').ToList();

            foreach (var value in sortList.Select(f => f.Split(":")))
            {
                switch (value[0])
                {
                    case sortParamByName:
                        SortParam = SortParamEnum.ByName;
                        break;
                    case sortParamByCode:
                        SortParam = SortParamEnum.ByCode;
                        break;
                    default:
                        SortParam = SortParamEnum.NoSort;
                        break;
                }

                try
                {
                    switch (value[1])
                    {
                        case sortValueDesc:
                            SortOrder = SortOrderEnum.Desc;
                            break;
                        default:
                            SortOrder = SortOrderEnum.Asc;
                            break;
                    }
                }
                catch
                {
                    // TODO: log exceptions somewhere
                }
            }

            if (page < 0)
                Page = NoPaging;
            else
                Page = page;
        }
    }
}