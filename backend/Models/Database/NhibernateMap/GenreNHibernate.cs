using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace library.Models.Database.NhibernateMap
{
    public class GenreNhibernate
    {
        public virtual int Id { get; set; }
        public virtual string Genre { get; set; }
    }
    
    public class GenreNhibernateMap : ClassMapping<GenreNhibernate>
    {
        public GenreNhibernateMap()
        {
            Table("genres");
            Schema("public");
            Lazy(true);
            
            Id(x => x.Id, m => m.Generator(Generators.Native));
            Property(x => x.Genre, map => map.NotNullable(true));
        }
    }
}