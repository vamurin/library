using System;
using System.Collections.Generic;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace library.Models.Database.NhibernateMap
{
    public class BookNhibernate
    {
        public virtual int Id { get; set; }
        public virtual string Isbn { get; set; }
        public virtual AuthorNhibernate Author { get; set; }
        public virtual string Name { get; set; }
        public virtual GenreNhibernate Genre { get; set; }
        public virtual PublishHouseNhibernate PublishHouse { get; set; }
        public virtual DateTime PublishYear { get; set; }
        public virtual string Description { get; set; }
    }
    
    public class BookNhibernateMap : ClassMapping<BookNhibernate>
    {
        public BookNhibernateMap()
        {
            Table("books");
            Schema("public");
            Lazy(true);
            
            Id(x => x.Id, m => m.Generator(Generators.Native));
            Property(x => x.Isbn, map => map.NotNullable(true));
            ManyToOne(m => m.Author, m =>
            {
                m.NotNullable(false);
                m.Column("author");
            });
            Property(x => x.Name, map => map.NotNullable(true));
            ManyToOne(m => m.Genre, m =>
            {
                m.NotNullable(false);
                m.Column("genre");
            });
            ManyToOne(m => m.PublishHouse, m =>
            {
                m.NotNullable(false);
                m.Column("publish_house");
            });
            Property(x => x.PublishYear, map => map.NotNullable(true));
            Property(x => x.Description, map => map.NotNullable(true));
        }
    }
}