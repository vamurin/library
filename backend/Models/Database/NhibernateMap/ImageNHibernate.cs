using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace library.Models.Database.NhibernateMap
{
    public class ImageNhibernate
    {
        public virtual int Id { get; set; }
        public virtual BookNhibernate Book { get; set; }
        public virtual string Image { get; set; }
    }
    
    public class ImageNhibernateMap : ClassMapping<ImageNhibernate>
    {
        public ImageNhibernateMap()
        {
            Table("images");
            Schema("public");
            Lazy(true);
            
            Id(x => x.Id, m => m.Generator(Generators.Native));
            ManyToOne(m => m.Book, m =>
            {
                m.NotNullable(true);
                m.Column("book");
            });
            Property(x => x.Image, map => map.NotNullable(true));
        }
    }
}