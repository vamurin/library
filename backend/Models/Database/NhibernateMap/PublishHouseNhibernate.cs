using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace library.Models.Database.NhibernateMap
{
    public class PublishHouseNhibernate
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual int Code { get; set; }
        public virtual string Description { get; set; }
    }

    public class PublishHouseNhibernateMap : ClassMapping<PublishHouseNhibernate>
    {
        public PublishHouseNhibernateMap()
        {
            Table("publish_houses");
            Schema("public");
            Lazy(true);
            
            Filter("publishHouseCodeFilter", m => m.Condition("Code = :filterCode"));
            Filter("publishHouseNameFilter", m => m.Condition("Name ilike :filterName"));
            
            Id(x => x.Id, m => m.Generator(Generators.Native));
            Property(x => x.Name, map => map.NotNullable(true));
            Property(x => x.Address, map => map.NotNullable(true));
            Property(x => x.Code, map => map.NotNullable(true));
            Property(x => x.Description, map => map.NotNullable(true));
        }
    }
}