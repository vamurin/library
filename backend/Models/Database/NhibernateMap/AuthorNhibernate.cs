using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace library.Models.Database.NhibernateMap
{
    public class AuthorNhibernate
    {
        public virtual int Id { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual string Photo { get; set; }
    }
    
    public class AuthorNhibernateMap : ClassMapping<AuthorNhibernate>
    {
        public AuthorNhibernateMap()
        {
            Table("authors");
            Schema("public");
            Lazy(true);

            Id(x => x.Id, m => m.Generator(Generators.Native));
            Property(x => x.FirstName, map => map.NotNullable(true));
            Property(x => x.LastName, map => map.NotNullable(true));
            Property(x => x.MiddleName, map => map.NotNullable(false));
            Property(x => x.BirthDate, map => map.NotNullable(true));
            Property(x => x.Photo, map => map.NotNullable(true));
        }
    }
}