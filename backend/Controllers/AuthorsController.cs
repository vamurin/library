﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Authors;
using library.Models.Books;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using library.ServiceExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorStorage<AuthorNhibernate> _authorStorage;
        private readonly FileStorage _fileStorage;
        private readonly IImageStorage<ImageNhibernate, BookNhibernate> _imageStorage;
        private readonly IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> _bookStorage;

        public AuthorsController(IAuthorStorage<AuthorNhibernate> authorStorage,
            IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> bookStorage,
            IImageStorage<ImageNhibernate, BookNhibernate> imageStorage,
            FileStorage fileStorage)
        {
            _authorStorage = authorStorage;
            _fileStorage = fileStorage;
            _bookStorage = bookStorage;
            _imageStorage = imageStorage;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetAllAuthors([FromQuery] AuthorGetRequest request)
        {
            // looks up in the DB and returns a sorted list of authors
            var list = await _authorStorage.GetAll(new AuthorFilter(request.Filter, request.Sort, request.Page));

            if (list == null)
                return NotFound();

            var responceJSON = new List<AuthorGetResponce>();
            list.ForEach(authorDB => responceJSON.Add(new AuthorGetResponce(authorDB)));
            
            return JsonConvert.SerializeObject(responceJSON);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> NewAuthor([FromForm] AuthorPutRequest request)
        {
            // inserts a new author into the DB
            try
            {
                var authorDB = await request.ToNhibernate(_fileStorage);

                await _authorStorage.Add(authorDB);            

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetSingleAuthor(int id)
        {
            // looks up in the DB and returns a single author
            var authorDB = await _authorStorage.GetById(id);

            if (authorDB == null)
                return NotFound();

            var responceJSON = new AuthorGetResponce(authorDB);

            return JsonConvert.SerializeObject(responceJSON);
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateSingleAuthor(int id, [FromForm] AuthorPutRequest request)
        {
            // updates an author in the DB
            try
            {
                var authorDB = await _authorStorage.GetById(id);

                if (authorDB == null)
                    return NotFound();

                // remove the old file
                _fileStorage.RemoveFileByDbUrl(authorDB.Photo);

                authorDB = await request.ToNhibernate(_fileStorage);
                authorDB.Id = id;

                await _authorStorage.Update(authorDB);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status423Locked)]
        public async Task<ActionResult<string>> DeleteSingleAuthor(int id)
        {
            // removes an author from the DB
            try
            {
                var authorDB = await _authorStorage.GetById(id);

                if (authorDB == null)
                    return NotFound();

                // cant remove if there books dependent on it
                var dependentBooks = await _bookStorage.GetByAuthor(authorDB);
                if (dependentBooks != null && dependentBooks.Count > 0)
                    return StatusCode(423);

                // remove the old file
                _fileStorage.RemoveFileByDbUrl(authorDB.Photo);

                await _authorStorage.Delete(authorDB);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("randomfivebooks/{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetRandomFiveBooksByAuthor(int id)
        {
            var authorDB = await _authorStorage.GetById(id);

            if (authorDB == null)
                return NotFound();

            var booksSimilarGenreDB = await _bookStorage.GetRandomByAuthor(SystemDefinitions.maxRandomBooksByAuthor, authorDB);

            var responceJSON = new List<BookFilteredGetResponce>();
            booksSimilarGenreDB.ForEach(async book => {
                var imageDB = await _imageStorage.GetByBook(book);
                responceJSON.Add(new BookFilteredGetResponce(book,
                    (imageDB != null && imageDB.Count > 0) ? imageDB[0] : null));
            });

            return JsonConvert.SerializeObject(responceJSON);
        }
    }
}