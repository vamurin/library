﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Books;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using library.Models.Publishers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublishersController : ControllerBase
    {
        private readonly IPublishHouseStorage<PublishHouseNhibernate> _publishHouseStorage;
        private readonly IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> _bookStorage;
        private readonly IImageStorage<ImageNhibernate, BookNhibernate> _imageStorage;

        public PublishersController(IPublishHouseStorage<PublishHouseNhibernate> publishHouseStorage,
            IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> bookStorage,
            IImageStorage<ImageNhibernate, BookNhibernate> imageStorage)
        {
            _publishHouseStorage = publishHouseStorage;
            _bookStorage = bookStorage;
            _imageStorage = imageStorage;
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetAllPublishers([FromQuery] PublisherGetRequest request)
        {
            var list = await _publishHouseStorage.GetAll(new PublishHouseFilter(request.Filter, request.Sort, request.Page));

            if (list == null)
                return NotFound();

            var responceJSON = new List<PublisherGetResponse>();
            list.ForEach(publisherDB => responceJSON.Add(new PublisherGetResponse(publisherDB)));    

            return JsonConvert.SerializeObject(responceJSON);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<string> NewPublisher([FromForm] PublisherPutRequest request)
        {
            try
            {
                var publishHouseNH = request.ToNhibernate();   
                _publishHouseStorage.Add(publishHouseNH);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetSinglePublisher(int id)
        {
            // looks up in the DB and returns a single author
            var publishHouseNH = await _publishHouseStorage.GetById(id);

            if (publishHouseNH == null)
                return NotFound();

            var response = new PublisherGetResponse(publishHouseNH); 

            return JsonConvert.SerializeObject(response);
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<string> UpdateSinglePublisher(int id, [FromForm] PublisherPutRequest request)
        {
            // updates an author in the DB
            try
            {
                var publishHouseNH = request.ToNhibernate(); 
                
                publishHouseNH.Id = id;

                _publishHouseStorage.Update(publishHouseNH);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status423Locked)]
        public async Task<ActionResult<string>> DeleteSinglePublisher(int id)
        {
            // removes an author from the DB
            try
            {
                var publishHouseNH = await _publishHouseStorage.GetById(id);

                if (publishHouseNH == null)
                    return NotFound();

                // cant remove if there books dependent on it
                var dependentBooks = await _bookStorage.GetByPublishHouse(publishHouseNH);
                if (dependentBooks != null && dependentBooks.Count > 0)
                    return StatusCode(423);

                await _publishHouseStorage.Delete(publishHouseNH);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("latestfivebooks/{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetLatestFiveBooksByPublisher(int id)
        {
            var publishHouseNH = await _publishHouseStorage.GetById(id);

            if (publishHouseNH == null)
                return NotFound();

            var booksSimilarGenreDB = await _bookStorage.GetLatestByPublisher(SystemDefinitions.maxLatestBooksByPublisher, publishHouseNH);

            var responceJSON = new List<BookFilteredGetResponce>();
            booksSimilarGenreDB.ForEach(async book => {
                var imageDB = await _imageStorage.GetByBook(book);
                responceJSON.Add(new BookFilteredGetResponce(book,
                    (imageDB != null && imageDB.Count > 0) ? imageDB[0] : null));
            });

            return JsonConvert.SerializeObject(responceJSON);
        }
    }
}