﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Books;
using library.Models.Database.Filters;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using library.ServiceExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> _bookStorage;
        private readonly IAuthorStorage<AuthorNhibernate> _authorStorage;
        private readonly IPublishHouseStorage<PublishHouseNhibernate> _publisherStorage;
        private readonly IGenreStorage<GenreNhibernate> _genreStorage;
        private readonly IImageStorage<ImageNhibernate, BookNhibernate> _imageStorage;
        private readonly FileStorage _fileStorage;

        public BooksController(IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> bookStorage,
            IAuthorStorage<AuthorNhibernate> authorStorage,
            IPublishHouseStorage<PublishHouseNhibernate> publisherStorage,
            IGenreStorage<GenreNhibernate> genreStorage,
            IImageStorage<ImageNhibernate, BookNhibernate> imageStorage,
            FileStorage fileStorage)
        {
            _bookStorage = bookStorage;
            _authorStorage = authorStorage;
            _publisherStorage = publisherStorage;
            _fileStorage = fileStorage;
            _genreStorage = genreStorage;
            _imageStorage = imageStorage;
        }

        [HttpGet]
        public async Task<ActionResult<string>> GetAllBooks([FromQuery] BookFilteredGetRequest request)
        {
            // looks up in the DB and returns a sorted list of books
            var list = await _bookStorage.GetAll(new BookFilter(request.Filter, request.Sort, request.Page));

            if (list == null)
                return NotFound();

            var responceJSON = new List<BookFilteredGetResponce>();
            list.ForEach(async bookDB => {
                var imageDB = await _imageStorage.GetByBook(bookDB);
                responceJSON.Add(new BookFilteredGetResponce(bookDB, 
                    (imageDB != null && imageDB.Count > 0) ? imageDB[0] : null));
            });

            return JsonConvert.SerializeObject(responceJSON);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        async public Task<IActionResult> NewBook([FromForm] BookPutRequest request)
        {
            // inserts a new book into the DB
            try
            {
                var bookDB = await request.ToNhibernateBook(_authorStorage, _publisherStorage, _genreStorage);

                var imagesList = await request.ToNhibernateImages(bookDB, _fileStorage);

                await _bookStorage.Add(bookDB);

                if (imagesList != null)
                    imagesList.ForEach(x => _imageStorage.Add(x));

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetSingleBook(int id)
        {
            // looks up in the DB and returns a single book
            var bookDB = await _bookStorage.GetById(id);

            if (bookDB == null)
                return NotFound();

            var imagesDB = await _imageStorage.GetByBook(bookDB);
            var genresDB = await _genreStorage.GetAll();

            var responceJSON = new BookSpecificGetResponce(bookDB, imagesDB, genresDB);

            return JsonConvert.SerializeObject(responceJSON);
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateSingleBook(int id, [FromForm] BookPutRequest request)
        {
            // updates a book in the DB
            try
            {
                var bookDB = await _bookStorage.GetById(id);

                if (bookDB == null)
                    return NotFound();

                var imagesList = await _imageStorage.GetByBook(bookDB);

                // remove old images
                if (imagesList != null)
                {
                    imagesList.ForEach(imageDB =>
                    {
                        _fileStorage.RemoveFileByDbUrl(imageDB.Image);
                        _imageStorage.Delete(imageDB);
                    });
                }

                bookDB = await request.ToNhibernateBook(_authorStorage, _publisherStorage, _genreStorage);
                bookDB.Id = id;
                imagesList = await request.ToNhibernateImages(bookDB, _fileStorage);

                await _bookStorage.Add(bookDB);
                imagesList.ForEach(x => _imageStorage.Add(x));

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<string>> DeleteSingleBook(int id)
        {
            // removes a book from the DB
            try
            {
                var bookDB = await _bookStorage.GetById(id);

                if (bookDB == null)
                {
                    return NotFound();
                }

                var imagesList = await _imageStorage.GetByBook(bookDB);

                // remove old images
                if (imagesList != null)
                {
                    imagesList.ForEach(imageDB =>
                    {
                        _fileStorage.RemoveFileByDbUrl(imageDB.Image);
                        _imageStorage.Delete(imageDB);
                    }); 
                    
                }

                await _bookStorage.Delete(bookDB);

                return Ok();
            }
            catch
            {
                // TODO: log exceptions somewhere
                return BadRequest();
            }
        }

        [HttpGet("randomfivebooks/{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> GetRandomFiveBooksByGenre(int id)
        {
            var bookDB = await _bookStorage.GetById(id);

            if (bookDB == null)
                return NotFound();

            var booksSimilarGenreDB = await _bookStorage.GetRandomByGenre(SystemDefinitions.maxRandomBooksByGenre, bookDB.Genre);

            var responceJSON = new List<BookFilteredGetResponce>();
            booksSimilarGenreDB.ForEach(async book => {
                var imageDB = await _imageStorage.GetByBook(book);
                responceJSON.Add(new BookFilteredGetResponce(book,
                    (imageDB != null && imageDB.Count > 0) ? imageDB[0] : null));
            });

            return JsonConvert.SerializeObject(responceJSON);
        }
    }
}