﻿using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using library.Helpers;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using library.Models.Other;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> _bookStorage;
        private readonly IConfiguration _configuration;

        public SearchController(IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate> bookStorage, IConfiguration configuration)
        {
            _bookStorage = bookStorage;
            _configuration = configuration;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<string>> ExternalSearch(OtherLibBookRequest request)
        {
            var response = await GetBookFromLocalBase(request.ISBN);

            if (response == null) return NotFound();

            return response;
        }

        [HttpPost("local")]
        public async Task<ActionResult<string>> LocalSearch(OtherLibBookRequest request)
        {
            var localResponse = await GetBookFromLocalBase(request.ISBN);
            
            if (localResponse != null)
                return localResponse;

            var requestUrl = _configuration.GetConnectionString(SystemDefinitions.appSettingsExternalLibrary1);
            var externalResponse = GetBookFromExternalBase(JsonConvert.SerializeObject(request), requestUrl);

            if (externalResponse.Result != null)
            {
                return externalResponse.Result;
            }
            
            requestUrl = _configuration.GetConnectionString(SystemDefinitions.appSettingsExternalLibrary2);
            externalResponse = GetBookFromExternalBase(JsonConvert.SerializeObject(request), requestUrl);

            if (externalResponse.Result != null)
            {
                return externalResponse.Result;
            }

            return NotFound();
        }

        protected async Task<string> GetBookFromLocalBase(string isbn)
        {
            try
            {
                var item = await _bookStorage.GetByISBN(isbn);
                if (item == null)
                    return null;

                var response = new OtherLibBookResponce()
                {
                    LibraryName = "SV2N Library",
                    BookName = item.Name,
                    Url = $"http://{HttpContext.Request.Host.Host}/{_configuration.GetValue<string>(SystemDefinitions.appSettingsLocalLibURL)}{item.Id}"
                };
        
                return JsonConvert.SerializeObject(response);
            }
            catch
            {
                // TODO: log exceptions somewhere
                return null;
            }
        }
        
        protected async Task<string> GetBookFromExternalBase(string request, string requestUrl)
        {
            using (var client = new HttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUrl)
                {
                    Content = new StringContent(request, Encoding.UTF8, "application/json")
                };
        
                var response = await client.SendAsync(requestMessage);
        
                var textResult = await response.Content.ReadAsStringAsync();

                return response.StatusCode == System.Net.HttpStatusCode.OK ? textResult : null;
            }
        }

 
    }
}