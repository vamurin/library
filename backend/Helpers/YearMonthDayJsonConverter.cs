﻿using Newtonsoft.Json.Converters;

namespace library.backend.Helpers
{
    public class YearMonthDayJsonConverter : IsoDateTimeConverter
    {
        public YearMonthDayJsonConverter()
        {
            base.DateTimeFormat = "yyyy.MM.dd";
        }

    }
}
