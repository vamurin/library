﻿using Newtonsoft.Json.Converters;

namespace library.backend.Helpers
{
    public class YearJsonConverter : IsoDateTimeConverter
    {
        public YearJsonConverter()
        {
            base.DateTimeFormat = "yyyy";
        }

    }
}
