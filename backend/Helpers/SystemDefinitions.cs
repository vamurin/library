﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library.Helpers
{
    public static class SystemDefinitions
    {
        public const string appSettingsRowsPerPage = "RowsPerPage";
        public const string appSettingsLibraryDatabase = "LibraryDatabase";
        public const string appSettingsExternalLibrary1 = "ExternalLibrary1";
        public const string appSettingsExternalLibrary2 = "ExternalLibrary2";
        public const string appSettingsFileStorage = "FileStorage";
        public const string appSettingsContentPath = "ContentPath";
        public const string appSettingsImagesDir = "ImagesDir";
        public const string appSettingsAllowedHosts = "AllowedHosts";
        public const string appSettingsLocalLibURL = "localLibraryURL";
        public const string authorBirthDateFormat = "yyyy.MM.dd";
        public const string bookPublishDateFormat = "yyyy";
        public const int maxRandomBooksByGenre = 5;
        public const int maxRandomBooksByAuthor = 5;
        public const int maxLatestBooksByPublisher = 5;
    }
}
