using FluentMigrator;
using System.IO;

namespace library.ServiceExtension
{
    [Migration(1)]
    public class BaseMigration : ForwardOnlyMigration
    {
        public override void Up()
        {
            CreateAndFillGenres();
            CreateAndFillAuthors();
            CreateAndFillPublishHouses();
            CreateAndFillBooks();
            CreateAndFillImages();
        }

        private void CreateAndFillGenres()
        {
            Create.Sequence("genres_id");
            Create.Table("genres")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("genres_id"))
                .WithColumn("genre").AsString().NotNullable();
            Create.UniqueConstraint()
                .OnTable("genres").Column("genre");

            Insert.IntoTable("genres").Row(new {genre = "Фантастика"});
            Insert.IntoTable("genres").Row(new {genre = "Детектив"});
            Insert.IntoTable("genres").Row(new {genre = "Роман"});
            Insert.IntoTable("genres").Row(new {genre = "Сказка"});
            Insert.IntoTable("genres").Row(new {genre = "Приключения"});
            Insert.IntoTable("genres").Row(new { genre = "Басня" });
            Insert.IntoTable("genres").Row(new { genre = "Баллада" });
            Insert.IntoTable("genres").Row(new { genre = "Комедия" });
            Insert.IntoTable("genres").Row(new { genre = "Стихотворение" });
            Insert.IntoTable("genres").Row(new { genre = "Мелодрама" });
            Insert.IntoTable("genres").Row(new { genre = "Повесть" });
            Insert.IntoTable("genres").Row(new { genre = "Поэма" });
            Insert.IntoTable("genres").Row(new { genre = "Рассказ" });
            Insert.IntoTable("genres").Row(new { genre = "Трагедия" });
            Insert.IntoTable("genres").Row(new { genre = "Эссе" });
        }

        private void CreateAndFillAuthors()
        {
            Create.Sequence("authors_id");
            Create.Table("authors")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("authors_id"))
                .WithColumn("first_name").AsAnsiString().NotNullable()
                .WithColumn("last_name").AsAnsiString().NotNullable()
                .WithColumn("middle_name").AsAnsiString().Nullable()
                .WithColumn("birth_date").AsDate().NotNullable()
                .WithColumn("photo").AsAnsiString().NotNullable();
            Create.UniqueConstraint()
                .OnTable("authors").Column("photo");

            Insert.IntoTable("authors").Row(new
            {
                first_name = "Джордж", middle_name = "Реймонд Ричард", last_name = "Мартин", birth_date = "20.09.1948",
                photo = Path.Combine("Content", "Images", "Martin.jpg")
            });;
            Insert.IntoTable("authors").Row(new
            {
                first_name = "Агата", last_name = "Кристи", birth_date = "15.09.1890",
                photo = Path.Combine("Content", "Images", "Christie.jpg")
            });
            Insert.IntoTable("authors").Row(new
            {
                first_name = "Шарлота", last_name = "Бронте", birth_date = "21.04.1816",
                photo = Path.Combine("Content", "Images", "Bronte.jpg")
            });
            Insert.IntoTable("authors").Row(new
            {
                first_name = "Ганс", middle_name = "Христиан", last_name = "Андерсен", birth_date = "02.04.1805",
                photo = Path.Combine("Content", "Images", "Andersen.jpg")
            });
            Insert.IntoTable("authors").Row(new
            {
                first_name = "Александр", last_name = "Дюма", birth_date = "24.07.1802",
                photo = Path.Combine("Content", "Images", "Dumas.jpg")
            });
        }

        private void CreateAndFillPublishHouses()
        {
            Create.Sequence("publish_house_id");
            Create.Table("publish_houses")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("publish_house_id"))
                .WithColumn("name").AsAnsiString().NotNullable()
                .WithColumn("address").AsAnsiString().NotNullable()
                .WithColumn("code").AsInt32().NotNullable()
                .WithColumn("description").AsAnsiString().Nullable();
            Create.UniqueConstraint()
                .OnTable("publish_houses").Column("code");

            Insert.IntoTable("publish_houses").Row(new {name = "Издательство АСТ", address = "г. Москва, Пресненская наб., д.6, стр.2, БЦ «Империя»", code = "10"});
            Insert.IntoTable("publish_houses").Row(new {name = "РОСМЭН", address = "г. Москва, ул. Октябрьская, д. 4 к 2, ст. м. Достоевская", code = "20"});
            Insert.IntoTable("publish_houses").Row(new {name = "Издательство «ЭКСМО»", address = "г. Москва, ул. Зорге, д.1.", code = "30"});
        }

        private void CreateAndFillBooks()
        {
            Create.Sequence("books_id");
            Create.Table("books")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("books_id"))
                .WithColumn("isbn").AsAnsiString().NotNullable()
                .WithColumn("author").AsInt32().NotNullable().ForeignKey("authors", "id")
                .WithColumn("name").AsAnsiString().NotNullable()
                .WithColumn("genre").AsInt32().NotNullable().ForeignKey("genres", "id")
                .WithColumn("publish_house").AsInt32().NotNullable().ForeignKey("publish_houses", "id")
                .WithColumn("publish_year").AsDateTime().NotNullable()
                .WithColumn("description").AsAnsiString().NotNullable();
            Create.UniqueConstraint()
                .OnTable("books").Column("isbn");

            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000000", author = "1", name = "Игра престолов", genre = "1", publish_house = "1",
                publish_year = "01.01.1996",
                description =
                    "Джордж Мартин - «живой классик» мировой фантастики, талантливейший из современных мастеров фэнтези, чьи произведения удостоены самых высоких наград жанра. Его шедевром по праву считается эпопея «Песнь льда и огня» - величайшая фэнтези-сага со времен Толкина!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000001", author = "1", name = "Битва королей", genre = "1", publish_house = "1",
                publish_year = "01.01.998",
                description =
                    "Джордж Мартин - «живой классик» мировой фантастики, талантливейший из современных мастеров фэнтези, чьи произведения удостоены самых высоких наград жанра. Его шедевром по праву считается эпопея «Песнь льда и огня» - величайшая фэнтези-сага со времен Толкина!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000002", author = "1", name = "Буря мечей", genre = "1", publish_house = "1",
                publish_year = "01.01.2000",
                description =
                    "Джордж Мартин - «живой классик» мировой фантастики, талантливейший из современных мастеров фэнтези, чьи произведения удостоены самых высоких наград жанра. Его шедевром по праву считается эпопея «Песнь льда и огня» - величайшая фэнтези-сага со времен Толкина!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000003", author = "1", name = "Пир стервятников", genre = "1", publish_house = "1",
                publish_year = "01.01.2005",
                description =
                    "Джордж Мартин - «живой классик» мировой фантастики, талантливейший из современных мастеров фэнтези, чьи произведения удостоены самых высоких наград жанра. Его шедевром по праву считается эпопея «Песнь льда и огня» - величайшая фэнтези-сага со времен Толкина!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000004", author = "1", name = "Танец с драконами", genre = "1", publish_house = "1",
                publish_year = "01.01.2011",
                description =
                    "Джордж Мартин - «живой классик» мировой фантастики, талантливейший из современных мастеров фэнтези, чьи произведения удостоены самых высоких наград жанра. Его шедевром по праву считается эпопея «Песнь льда и огня» - величайшая фэнтези-сага со времен Толкина!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000005", author = "2", name = "Убийство в «Восточном экспрессе»", genre = "2",
                publish_house = "1", publish_year = "01.01.1934",
                description =
                    "Даже если подозреваемых слишком много, а мотивы преступления неясны, гениальный сыщик Эркюль Пуаро найдет правильный путь в лабиринте криминальной интриги. Он безошибочно определяет убийцу в поезде."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000006", author = "2", name = "Десять негритят", genre = "2", publish_house = "1",
                publish_year = "01.01.1939",
                description =
                    "Десять никак не связанных между собой людей в особняке на уединенном острове... Кто вызвал их сюда таинственным приглашением? Зачем кто-то убивает их, одного за другим, самыми невероятными способами? Почему все происходящее так тесно переплетено с веселым детским стишком?"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000007", author = "2", name = "Загадка Эндхауза", genre = "2", publish_house = "3",
                publish_year = "01.01.1932",
                description =
                    "Отдыхая на корнуолльском побережье со своим верным помощником капитаном Гастингсом, великий сыщик Эркюль Пуаро случайно узнает, что богатая хозяйка большого поместья Эндхауз, расположенного неподалеку от их отеля, прекрасная Ник Бакли жалуется на смертельную опасность, якобы угрожающую ей. Сначала, ни с того ни с сего, выходят из строя тормоза на ее автомобиле. Затем, на горной тропе, буквально в сантиметрах от нее пролетает упавший сверху валун. И, в довершение всего, на женщину обрушивается со стены большая тяжелая картина. Наконец, когда Пуаро обнаруживает отверстие от пули в летней шляпке Ник, он решает, что опасность действительно существует, и начинает расследование. Но его результаты поразят даже такого искушенного в преступлениях человека, как Пуаро…"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000008", author = "2", name = "Смерть на Ниле", genre = "2", publish_house = "3",
                publish_year = "01.01.1937",
                description =
                    "На роскошном пароходе «Карнак», плывущем по Нилу, убита молодая миллионерша, недавно вышедшая замуж и, как выяснилось, имевшая множество врагов среди пассажиров. Любой мог убить самоуверенную и нагловатую девушку, укравшую жениха у лучшей подруги. Но ни один из вероятных подозреваемых не совершал этого преступления… К счастью, на пароходе находится великий сыщик Эркюль Пуаро, который знает все общество, представленное в круизе, еще по Лондону, и в курсе возможных мотивов каждого из присутствующих. И, конечно, первое, о чем задумывается бельгиец, - это о «любовном треугольнике», состоявшем из убитой, ее свежеиспеченного мужа и очень темпераментной женщины, которую тот бросил ради миллионерши…"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000009", author = "2", name = "Рождество Эркюля Пуаро", genre = "2", publish_house = "1",
                publish_year = "01.01.1939",
                description =
                    "В Рождество происходит убийство главы семейства. Под подозрением сыновья убитого, их жены, внучка, лакей и гость главы семьи. Пуаро оказывается неподалеку — в гостях у местного комиссара полиции."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000010", author = "3", name = "Джейн Эйр", genre = "3", publish_house = "3",
                publish_year = "01.01.1847",
                description =
                    "Всемирно известный роман Шарлотты Бронте о невзрачной девочке, чей сильный дух не был сломлен под гнетом бессердечных детей миссис Рид и жестоких порядков Ловудской школы-приюта. Став гувернанткой в мрачном поместье Торнфильд, Джейн и представить не могла, сколько еще тайн ей придется разгадать, прежде чем обрести долгожданное счастье."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000011", author = "3", name = "Городок", genre = "3", publish_house = "1",
                publish_year = "01.01.1853",
                description =
                    "14-летняя девочка, Люси Сноу, гостит у своей крёстной в городке Бреттон. В этом доме судьба сводит её с двумя удивительными людьми — Грэмом Бреттоном, сыном её крёстной, и Полиной Хоум, серьёзной миниатюрной девочкой. Когда Люси взрослеет, она уезжает в Брюссель, чтобы работать учительницей в пансионате. Позже она встречает уже повзрослевших друзей детства — Грэма и Полину. Поначалу Люси переписывается с Грэмом и налаживает нежные, дружественные отношения с ним, однако Грэм, вновь встретив Полину, уже взрослую и обаятельную девушку, решает жениться именно на ней. Люси заставляет себя не навязываться Грэму, а оставить их редкие беседы как есть, и влюбляется в другого учителя много старше её, Поля Эманюэля, хотя поначалу он казался ей суровым и непривлекательным «коротышкой». В конце романа он делает ей сюрприз — готовит арендованное помещение под школу, и уезжает на несколько лет из страны. Однако, по дороге домой его корабль терпит бедствие, и он погибает."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000012", author = "3", name = "Учитель", genre = "3", publish_house = "1",
                publish_year = "01.01.1857",
                description =
                    "Книга рассказывает историю молодого человека, Уильяма Кримсворта. Описывается его взросление, любовь и карьера в качестве учителя в школе для девочек."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000013", author = "4", name = "Русалочка", genre = "4", publish_house = "2",
                publish_year = "01.01.1837",
                description =
                    "Юная Русалочка бесстрашно окунается в океан любви, бескрайний и опасный как сама жизнь. Ее сердце полно отваги, надежды и бесконечной преданности. Свой волшебный голос и беспечную жизнь морской принцессы она готова обменять на минуты счастья рядом с любимым. Ведь только ответная любовь может подарить ей величайшие сокровища - бессмертную душу и вечную жизнь."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000014", author = "4", name = "Гадкий утенок", genre = "4", publish_house = "2",
                publish_year = "01.01.1843",
                description =
                    "Наверное, из всех историй, написанных датским сказочником Гансом Христианом Андерсеном, у «Гадкого утёнка» самый счастливый конец. Может быть, поэтому её так любят и дети, и взрослые. Ведь не только мальчикам и девочкам хочется верить, что Золушек ждут Прекрасные Принцы, а из Гадких Утят вырастают Прекрасные Лебеди!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000015", author = "4", name = "Дюймовочка", genre = "4", publish_house = "2",
                publish_year = "01.01.1835",
                description =
                    "Сказки Андерсена полны доброты и светлой грусти, они дают детям ощущение чуда, имя которому - истинное человеческое благородство, сострадание к ближним, любовь и справедливость. Перед вами история крошечной девочки, появившейся из цветочного бутона. Пройдя через множество опасностей, она попала в страну эльфов и получила в подарок прекрасные крылья."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000016", author = "4", name = "Принцеса на горошине", genre = "4", publish_house = "2",
                publish_year = "01.01.1835",
                description =
                    "Принц объехал весь свет в поисках принцессы, но не нашёл ни одной настоящей. И вот однажды в ненастный вечер на пороге королевского замка появилась принцесса. Но как проверить, настоящая ли она? У королевы есть способ: подложить девушке под сорок тюфяков одну-единственную маленькую горошину. Только истинная принцесса сможет ощутить её!"
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000017", author = "4", name = "Снежная королева", genre = "4", publish_house = "2",
                publish_year = "01.01.1844",
                description =
                    "Острые двускатные крыши домов и черепичные кровли, засыпанные снегом, сгущающиеся над городом темные тучи, блеск и переливы льда… Это мир, в котором властвует андерсеновская Снежная королева. В этом мире люди выглядят маленькими и как бы второстепенными… Но они умеют сострадать и любить, быть верными и самоотверженными - и поэтому могут вырваться из плена королевы и растопить осколок колдовского зеркала, превращающего любое сердце в лед."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000018", author = "5", name = "Три мушкетера", genre = "5", publish_house = "3",
                publish_year = "01.01.1844",
                description =
                    "Это лучший авантюрно-приключенческий роман в истории литературы. Проходит время, однако эта прекрасная книга по-прежнему популярна, история об отважных мушкетерах продолжает вдохновлять режиссеров на создание ярких экранизаций, и вот уже новое поколение читателей и зрителей с замиранием сердца следит за увлекательными и опасными похождениями бесстрашного гасконца д'Артаньяна, благородного Атоса, шумного Портоса и загадочного Арамиса..."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000019", author = "5", name = "Королева Марго", genre = "5", publish_house = "3",
                publish_year = "01.01.1845",
                description =
                    "Описанные в книге исторические события относятся к эпохе гражданских войн во Франции конца XVI столетия. Увлекательный сюжет, бурный ритм развития действия, общая оптимистическая атмосфера романа, жизнеутверждающий юмор свидетельствуют о замечательном мастерстве Дюма-рассказчика."
            });
            Insert.IntoTable("books").Row(new
            {
                isbn = "0000000020", author = "5", name = "Граф Монте-Кристо", genre = "5", publish_house = "3",
                publish_year = "01.01.1846",
                description =
                    "Как и сто шестьдесят пять лет назад, «Граф Монте-Кристо» Александра Дюма остается одним из самых популярных романов в мировой литературе. К нему писали продолжения, его ставили на сцене, создавали мюзиклы, экранизировали, но и по сей день бесчисленные издания этой книги доставляют удовольствие новым и новым поколениям читателей. История молодого парижанина, которого приятели в шутку засадили в тюрьму, почерпнута автором в архивах парижской полиции. А из-под пера мастера выходит моряк Эдмон Дантес, мученик замка Иф. Не дождавшись правосудия, он решает сам вершить суд и жестоко мстит врагам, разрушившим его счастье. "
            });
        }

        public void CreateAndFillImages()
        {
            Create.Sequence("images_id");
            Create.Table("images")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("images_id"))
                .WithColumn("image").AsString().NotNullable()
                .WithColumn("book").AsInt32().NotNullable().ForeignKey("books", "id");

            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "GameOfThrones.jpg"), book = "1"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ASongOfIceAndFire.jpg"), book = "1"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ClashOfKings.jpg"), book = "2"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ASongOfIceAndFire.jpg"), book = "2"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "StormOfSwords.jpg"), book = "3"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ASongOfIceAndFire.jpg"), book = "3"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "FeastForCrows.jpg"), book = "4"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ASongOfIceAndFire.jpg"), book = "4"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "DanceWithDragons1.jpg"), book = "5"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "DanceWithDragons2.jpg"), book = "5"});
            Insert.IntoTable("images").Row(new {image = Path.Combine("Content", "Images", "ASongOfIceAndFire.jpg"), book = "5"});
        }
    }

    public class NextVal
    {
        private readonly string _name;

        public NextVal(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return $"nextval('{_name}')";
        }
    }
}