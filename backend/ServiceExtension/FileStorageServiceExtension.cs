﻿using library.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace library.ServiceExtension
{
    public static class FileStorageExtension
    {
        public static void AddFileStorage(this IServiceCollection services)
        {
            services.AddScoped<FileStorage>();
        }
    }

    public class FileStorage
    {
        private readonly IConfiguration _conf;

        public FileStorage(IConfiguration conf)
        {
            _conf = conf;
        }

        async public Task<string> WriteFormFile(IFormFile file)
        {
            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);


            var relativePath = Path.Combine(_conf.GetValue<string>($"{SystemDefinitions.appSettingsFileStorage}:{SystemDefinitions.appSettingsContentPath}"),
                _conf.GetValue<string>($"{SystemDefinitions.appSettingsFileStorage}:{SystemDefinitions.appSettingsImagesDir}"),
                fileName);
            var fullPath = Path.Combine(Directory.GetCurrentDirectory(), relativePath);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return relativePath;
        }

        public void RemoveFileByDbUrl(string fileDbUrl)
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), fileDbUrl)))
            {
                File.Delete(Path.Combine(Directory.GetCurrentDirectory(), fileDbUrl));
            }
        }
    }
}
