using System.Collections.Generic;
using System.Reflection;
using library.Models.Database.Implementations;
using library.Models.Database.Interfaces;
using library.Models.Database.NhibernateMap;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Engine;
using NHibernate.Mapping.ByCode;
using NHibernate.Type;

namespace library.ServiceExtension
{
    public static class NHibernateServiceExtension
    {
        public static void AddNhibernate(this IServiceCollection services, string conn)
        {
            var config = new NHibernate.Cfg.Configuration()
                .SetNamingStrategy(ImprovedNamingStrategy.Instance)
                .DataBaseIntegration(db =>
                {
                    db.ConnectionString = conn;
                    db.Dialect<PostgreSQL83Dialect>();
                    db.LogFormattedSql = true;
                    db.LogSqlInConsole = true;
                });
            var publishHouseCodeFilter = new FilterDefinition("publishHouseCodeFilter", null, 
                new Dictionary<string, IType> {{"filterCode", NHibernateUtil.Int32}},  false);
            var publishHouseNameFilter = new FilterDefinition("publishHouseNameFilter", null, 
                new Dictionary<string, IType> {{"filterName", NHibernateUtil.String}},  false);
            config.AddFilterDefinition(publishHouseCodeFilter);
            config.AddFilterDefinition(publishHouseNameFilter);

            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetAssembly(typeof(AuthorNhibernateMap)).GetExportedTypes());
            mapper.AddMappings(Assembly.GetAssembly(typeof(PublishHouseNhibernateMap)).GetExportedTypes());
            mapper.AddMappings(Assembly.GetAssembly(typeof(BookNhibernateMap)).GetExportedTypes());
            mapper.AddMappings(Assembly.GetAssembly(typeof(GenreNhibernateMap)).GetExportedTypes());
            mapper.AddMappings(Assembly.GetAssembly(typeof(ImageNhibernateMap)).GetExportedTypes());
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            
            config.AddMapping(mapping);
            config.SessionFactory().GenerateStatistics();

            services.AddSingleton(f => config.BuildSessionFactory());
            services.AddScoped(m => m.GetService<ISessionFactory>().OpenSession());
            services.AddScoped<IBookStorage<BookNhibernate, AuthorNhibernate, PublishHouseNhibernate, GenreNhibernate>, DbBookStorage>();
            services.AddScoped<IAuthorStorage<AuthorNhibernate>, DbAuthorStorage>();
            services.AddScoped<IGenreStorage<GenreNhibernate>, DbGenreStorage>();
            services.AddScoped<IImageStorage<ImageNhibernate, BookNhibernate>, DbImageStorage>();
            services.AddScoped<IPublishHouseStorage<PublishHouseNhibernate>, DbPublishHouseStorage>();
        }
    }
}