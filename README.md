# library

*Инструкция по установке*

1. Установить PostgreSQL командами:

sudo yum install postgresql-server postgresql-contrib

sudo postgresql-setup initdb

sudo systemctl start postgresql

sudo systemctl enable postgresql


2. В файле /var/lib/pgsql/data/pg_hba.conf выставить следующие строки:

host all all 127.0.0.1/32 md5


3. Выставить в файле /var/lib/pgsql/data/postgresql.conf выставить параметр:

datestyle = 'iso, dmy'


3. Перезапустить PostgreSQL:

sudo systemctl restart postgresql.service


4. Установить пароль для пользователя postgres и создать БД

sudo -u postgres psql template1

ALTER USER postgres with encrypted password '123';

CREATE DATABASE db_library;


В приложении по умолчанию используется пароль 123.

В приложении по умолчанию используется БД db_library.

Строка подключения с логином, паролем и именем БД указана в файле приложения appsettings.json, параметр "ConnectionStrings"->"LibraryDatabase".


5. Установить dotnet с помощью инструкции:

https://docs.microsoft.com/en-us/dotnet/core/install/linux-package-manager-centos7


6. Выполнить команду, чтобы разрешить nginx редиректить на ASP NET

setsebool -P httpd_can_network_connect 1


7. Сбилдить и опубликовать library.sln (в папке backend).

dotnet publish -c Release -r linux-x64 --self-contained true

8. Запустить приложение ./library

9. Сервис для запуска/остановки бэкенда называется backend-mfi-library.service

sudo systemctl enable backend-mfi-library.service

